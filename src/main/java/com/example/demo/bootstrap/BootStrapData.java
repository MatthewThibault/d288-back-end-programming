package com.example.demo.bootstrap;

import com.example.demo.dao.CustomerRepository;
import com.example.demo.entities.Customer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private CustomerRepository customerRepository;

    public BootStrapData(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        int customerNum = 0;
        customerNum = (int) customerRepository.count();
        if(customerNum<=1){
            //Sample customers
            Customer customerA = new Customer("Jake", "State", "123 Main Street", "12345", "(123)-456-7890");
            Customer customerB = new Customer("Blake", "Blake", "321 Side Street", "54321", "(504)-504-5045");
            Customer customerC = new Customer("Lake", "Slate", "1500 Easy Street", "10101", "(101)-101-1010");
            Customer customerD = new Customer("Drake", "Date", "101 My Way", "99999", "(999)-999-9999");
            Customer customerE = new Customer("Late", "Plate", "601 Turkey Avenue", "67676", "(676)-012-6789");

            //Save customer to repository
            customerRepository.save(customerA);
            customerRepository.save(customerB);
            customerRepository.save(customerC);
            customerRepository.save(customerD);
            customerRepository.save(customerE);

            System.out.println("Started in Bootstrap");
            System.out.println("Number of Customers: "+customerRepository.count());



        }
    }
}
